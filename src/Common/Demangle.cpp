#include <memory>
#include <string>
#include <cxxabi.h>

#include <Common/Demangle.hpp>

namespace common
{

std::string demangle(const char *name) {

  int status = -4;

  std::unique_ptr<char, void (*)(void *)> res{
      abi::__cxa_demangle(name, NULL, NULL, &status), free};

  return (status == 0) ? res.get() : name;
}

}  // namespace common
