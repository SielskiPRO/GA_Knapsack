#include <Algorithms/GeneticAlgorithm.hpp>
#include <iostream>

namespace algorithms
{

GeneticAlgorithm::GeneticAlgorithm()
{
    std::cout << "ga constructor" << std::endl;
}

void GeneticAlgorithm::execute()
{
    std::cout << "ga execute" << std::endl;
}

void GeneticAlgorithm::clear()
{
    std::cout << "ga clear" << std::endl;
}

}  // namespace algorithm