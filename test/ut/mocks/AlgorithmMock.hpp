#include <gmock/gmock.h>
#include <Algorithms/IAlgorithm.hpp>

namespace mocks
{

class IAlgorithmMock : public algorithms::IAlgorithm
{
public:
    MOCK_METHOD0(execute, void());
    MOCK_METHOD0(clear, void());
};


}  // namespace mocks
