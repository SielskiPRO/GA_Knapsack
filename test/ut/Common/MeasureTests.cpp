#include <AlgorithmMock.hpp>
#include <Common/Measure.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace test
{

using ::testing::AtLeast; 
using namespace mocks;

TEST(MeasureShould, executeAlgorithm)
{
    IAlgorithmMock algorithm;
    EXPECT_CALL(algorithm, execute())
        .Times(1);

    common::Measure<>::execution(algorithm.execute, algorithm);
}

}  // namespace test