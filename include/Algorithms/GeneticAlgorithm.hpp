#ifndef ALGORITHMS_GENETICALGORITHM_HPP
#define ALGORITHMS_GENETICALGORITHM_HPP

#include "IAlgorithm.hpp"

namespace algorithms
{

class GeneticAlgorithm : public IAlgorithm
{
public:
    explicit GeneticAlgorithm();
    void execute() override;
    void clear() override;
    virtual ~GeneticAlgorithm() {}
};

}  // namespace algorithm
#endif  // ALGORITHMS_GENETICALGORITHM_HPP