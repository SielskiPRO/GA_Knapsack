#ifndef IALGORITHM_HPP
#define IALGORITHM_HPP

namespace algorithms
{

class IAlgorithm
{
public:
    virtual void execute() = 0;
    virtual void clear() = 0;
    virtual ~IAlgorithm() {};
};

}  // namespace algorithm

#endif  // IALGORITHM_HPP
