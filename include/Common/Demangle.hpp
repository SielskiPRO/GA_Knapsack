#ifndef COMMON_DEMANGLE_HPP
#define COMMON_DEMANGLE_HPP

#include <string>

namespace common
{

std::string demangle(const char* name);

}  // namespace common

#endif  // COMMON_DEMANGLE_HPP
