#ifndef COMMON_MEASURE_HPP
#define COMMON_MEASURE_HPP

#include <chrono>
#include <utility>
#include <type_traits>

namespace common
{

template <typename TimeT = std::chrono::milliseconds>
struct Measure
{
    template <typename Function, typename Object, typename ...Args>
    static std::enable_if_t<std::is_class<Object>::value, typename TimeT::rep> 
    execution(Function func, Object& o, Args&&... args)
    {
        auto start = std::chrono::steady_clock::now();
        (o.*func)(std::forward<Args>(args)...);
        auto duration = std::chrono::duration_cast<TimeT>
            (std::chrono::steady_clock::now() - start);
        return duration.count();
    }

    template <typename Function, typename ...Args>
    static typename TimeT::rep execution(Function&& func, Args&&... args)
    {
        auto start = std::chrono::steady_clock::now();
        std::forward<decltype(func)>(func)(std::forward<Args>(args)...);
        auto duration = std::chrono::duration_cast<TimeT>
            (std::chrono::steady_clock::now() - start);
        return duration.count();
    }
};

}  // namespace common

#endif  // COMMON_MEASURE_HPP
