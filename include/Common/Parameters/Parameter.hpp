#ifndef COMMON_PARAMETERS_PARAMETER_HPP
#define COMMON_PARAMETERS_PARAMETER_HPP

#include <string>
#include <iostream>
#include <type_traits>
#include <utility>

namespace common
{
namespace parameters
{

template <typename ValueType> class Parameter;

template <typename ValueType>
auto make_parameter(const std::string& name, ValueType&& value)
{
    return Parameter<ValueType>(name, std::forward<ValueType>(value));
}

template <typename ValueType>
std::ostream& operator<< (std::ostream& os, const Parameter<ValueType>& parameter);

template <typename ValueType>
class Parameter
{
public:
    typedef ValueType value_type;

    template <typename T>
    explicit Parameter(const std::string& name, const T& value)
    {
        static_assert(std::is_same<T, Parameter::value_type>::value, "Types are not the same");
        this->name = name;
        this->value = value;
    }
    const std::string& getName() const;
    const value_type& getValue() const;
    friend std::ostream& operator<< <>(std::ostream& os, const Parameter& parameter);

private:
    std::string name;
    ValueType value;
};

template <typename T>
const std::string& Parameter<T>::getName() const
{
    return name;
}

template <typename T>
const T& Parameter<T>::getValue() const
{
    return value;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const Parameter<T>& parameter)
{
    os << parameter.getName() << " " <<parameter.getValue() << std::endl;
    return os;
}

}  // parameters
}  // common

#endif  // COMMON_PARAMETERS_PARAMETER_HPP
